# Branches

Each branch name is coded as `$model-$distro-$hostname` and contains machine 
specific settings.

Available branches:

- `lemp10-arch-lambda` (main)
- `pbp-manjaro-phi` (unmaintained)

# Install

Run `./install` to install setup's requirements.

What it does:

1. install needed packages
2. fetch wallpaper, icon pack and gtk theme
3. apply system configurations (`sudo ./apply -d etc`)
4. apply user configurations (`./apply home`)

# Maintenance

## Re-apply configurations

Run `./apply [PKG]` to apply `PKG` configurations to the current machine. 
Available packages:

- `etc`: system configurations (requires `sudo`)
- `home`: user configutations

By default, `home` package will be applied.

The script will recreate the package's directory tree and will try to copy 
its files over the machine. If the target file already exists, it will ask 
to overwrite it, launch `vimdiff` or skip the operation.

It's possible to force overwrite by invoking `./apply -f [PKG]`.

It's possible to force diff behavior by invoking `./apply -d [PKG]`.

## Merge edits from host

Run `./apply -d [PKG]` to force diff behavior.

It scans `PKG` tree and checks if files differ from their target, 
running `vimdiff` if they do.

## Merge edits from another branch

Run `git checkout -p BRANCH_TO_MERGE` to run git in interactive merge mode.
