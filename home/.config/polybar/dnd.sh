#!/bin/sh -e

fmt="%%{A1:dunstctl set-paused toggle:}%s%%{A1}\n"

status() {
    paused="$(dunstctl is-paused)"
    if [ "$paused" == "true" ]; then
        printf "$fmt" "%{F#f3b43a}󰚣%{F-}"
    else
        printf "$fmt" "󰍩"
    fi
}

status
dbus-monitor | while IFS= read -r line; do
    if echo "$line" | grep -q 'string "org.dunstproject.cmd0"'; then
        status
    fi
done