#!/bin/sh -e
interactive="󰩬"
window=""
screen="󰍹"

menu() {
    printf "$interactive\n"
    printf "$window\n"
    printf "$screen\n"
}

apply() {
    case "$1" in
        $interactive) i3-shot -i;;
        $window) i3-shot -w;;
        $screen) i3-shot -s;;
    esac
}

apply "$(menu | rofi -dmenu -config ~/.config/rofi/shot.rasi)"
