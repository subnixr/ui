#!/bin/sh -e
lock=""
hibernate=""
poweroff="󰐥"
reboot="󰑓"
logout="󰍃"

#lock="L"
#hibernate="H"
#poweroff="P"
#reboot="R"
#logout="O"

menu() {
    printf "$lock\n"
    printf "$hibernate\n"
    printf "$poweroff\n"
    printf "$reboot\n"
    printf "$logout\n"
}

apply() {
    case "$1" in
        $lock) loginctl lock-session;;
        $hibernate) systemctl hibernate;;
        $poweroff) systemctl poweroff;;
        $reboot) systemctl reboot;;
        $logout) i3-msg exit;;
    esac
}

apply "$(menu | rofi -dmenu -config ~/.config/rofi/power.rasi)"
